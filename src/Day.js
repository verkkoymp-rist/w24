const Day = ({ month }) => {
    const year = 2021;
    const firstDay = new Date(year, month - 1, 1).getDay();
    const weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const weekday = weekdays[firstDay];
  
    return <div>Month {month}/2021 starts {weekday}</div>;
  };
  
  export default Day;
  
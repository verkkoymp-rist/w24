// App.js
import React from 'react';
import Month from './Month';

const App = () => {
  return (
    <div>
      <h1>Find start weekday of week</h1>
      <Month />
    </div>
  );
};

export default App;

import React, { useState } from 'react';
import Day from './Day';

const Month = () => {
  const [month, setMonth] = useState(1);
  return (
    <div>
      <label>
        Choose a month:
        <select value={month} onChange={(e) => setMonth(e.target.value)}>
          {Array.from({ length: 12 }, (_, i) => (
            <option key={i + 1} value={i + 1}>
              {i + 1}
            </option>
          ))}
        </select>
      </label>
      {}
      <Day month={month} />
    </div>
  );
};

export default Month;
